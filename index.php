<?php
/**
 * Plugin Name: Integrus Roistat Leads
 * Description: Создание Лидов через Roistat
 * Version: 0.1
 * Author: Yanush A.
 */

namespace Integrus\RoistatLeads;

if (!defined('ABSPATH'))
    exit; // Exit if accessed directly


$intEnqueues = plugin_dir_path(__FILE__) . 'Classes/IntEnqueues.php';
$intSendToRoistat = plugin_dir_path(__FILE__) . 'Classes/IntSendToRoistat.php';
$intSendToBitrix = plugin_dir_path(__FILE__) . 'Classes/IntSendToBitrix.php';

if (file_exists($intEnqueues)):
    require_once $intEnqueues;
else:
    error_log('File d`not exists - ' . plugin_dir_path(__FILE__) . $intEnqueues);
    end();
endif;

if (file_exists($intSendToRoistat)):
    require_once $intSendToRoistat;
else:
    error_log('File d`not exists - ' . plugin_dir_path(__FILE__) . $intSendToRoistat);
    end();
endif;

if (file_exists($intSendToBitrix)):
    require_once $intSendToBitrix;
else:
    error_log('File d`not exists - ' . plugin_dir_path(__FILE__) . $intSendToBitrix);
    end();
endif;

use Integrus\RoistatLeads;

if (!class_exists('IntIndexSendLeadsToRoistat')) {

    class IntIndexSendLeadsToRoistat
    {

        public $__intEnqueues,
                $__intSendToRoistat,
                $__intSendToBitrix;
        
        private $globalArray;
        
        private $NONCE,
                $OPTION_NAME;

        public function __construct() {

            add_action('admin_menu', array($this, 'addSettingsPage'));

            $this->NONCE = get_class($this) . '_nonce';
            $this->OPTION_NAME = get_class($this) . '_int_lead_option';

            $this->__intEnqueues = new IntEnqueues;
            $this->__intEnqueues->setNonce($this->NONCE);

            add_action('wp_ajax_selected_method', array($this, 'selectedMethod'));

            self::methodRoute();
        }

        /**
         * Выбираем каким класс для создания лидов
         */
        private function methodRoute() {

            if (isset($_POST['selectedMethod'])) {
                $method = $_POST['selectedMethod'];
            } else {

                $value = self::intGetOption('int_selected_method');
                $method = isset($value['selectedMethod']) ? $value['selectedMethod'] : false;
            }

            $value = self::intGetOption('int_selected_method');
            $value['error'] = false;


            if ($method == 'toRoistat') {

                error_log($method . ' == toRoistat');

                if (empty($value[$method]['handlerUrl']) || empty($value[$method]['apiKey'])) {
                    if (empty($value[$method]['handlerUrl']))
                        error_log('ERROR : API data empty "$this->__intSendToRoistat->setApiUrl"');

                    if (empty($value[$method]['apiKey']))
                        error_log('ERROR : API data empty "$this->__intSendToRoistat->setApiKey"');

                    $value['error_descr'] = 'Заполните обязательные поля API ключа и URL обработчика';
                    $value['error'] = true;
                }

                if (!$value['error']) {
                    $this->__intSendToRoistat = new IntSendToRoistat;
                    $this->__intSendToRoistat->setOptionName($this->OPTION_NAME);
                    $this->__intSendToRoistat->setApiUrl($value[$method]['handlerUrl']);
                    $this->__intSendToRoistat->setApiKey($value[$method]['apiKey']);
                }
            } elseif ($method == 'toBitrix24') {

                error_log($method . ' == toBitrix24');



                if (empty($value[$method]['handlerUrl']) || empty($value[$method]['apiKey'])) {
                    if (empty($value[$method]['handlerUrl']))
                        error_log('ERROR : API data empty "$this->__intSendToBitrix->setApiUrl"');

                    if (empty($value[$method]['apiKey']))
                        error_log('ERROR : API data empty "$this->__intSendToBitrix->setApiKey"');

                    $value['error_descr'] = 'Заполните обязательные поля API ключа и URL обработчика';
                    $value['error'] = true;
                }


                if (!$value['error']) {
                    error_log('1');
                    $this->__intSendToBitrix = new IntSendToBitrix;
                    error_log('2 obj');
                    $this->__intSendToBitrix->setOptionName($this->OPTION_NAME);
                    $resp = $this->__intSendToBitrix->setApiUrl($value[$method]['handlerUrl']);
                    $resp = $this->__intSendToBitrix->setApiKey($value[$method]['apiKey']);
                }
            } else {
                error_log($method . ' == else');
                $value['methodDescr'] = 'Метод лидо-генерации не выбран.';
                $value['error'] = true;
                self::intUpdateOption($value, 'int_selected_method');
            }
        }

        /**
         * 
         * @param type $opt_name
         * @return type
         */
        private function intGetOption($opt_name = '') {

            $opt_name = $opt_name != '' ? $opt_name : $this->OPTION_NAME;
            return unserialize(get_option($opt_name));
        }

        /**
         * 
         * @param type $value
         * @param type $opt_name
         * @return type
         */
        private function intUpdateOption($value = array(), $opt_name = '') {

            $opt_name = $opt_name != '' ? $opt_name : $this->OPTION_NAME;
            $res = update_option($opt_name, serialize($value));
            return $res;
        }

        /**
         * 
         */
        public function selectedMethod() {

            if (isset($_POST['selectedMethod'])) {

                $value = self::intGetOption('int_selected_method');

                $method = $_POST['selectedMethod'];
                $value['selectedMethod'] = $method;
                $value[$method]['apiKey'] = isset($_POST['apiKey']) ? esc_sql(trim($_POST['apiKey'])) : '';
                $value[$method]['handlerUrl'] = isset($_POST['handlerUrl']) ? esc_sql(trim($_POST['handlerUrl'])) : '';

                if ($method == 'toRoistat') {
                    $value['methodDescr'] = 'Лиды будут отправляться через <span>Roistat</span>.';
                    $value['error'] = false;
                } elseif ($method == 'toBitrix24') {
                    $value['methodDescr'] = 'Лиды будут отправляться в <span>Bitrix24</span> напрямую.';
                    $value['error'] = false;
                } else {
                    $value['methodDescr'] = 'Метод лидо-генерации не выбран.';
                    $value['error'] = true;
                }

                self::intUpdateOption($value, 'int_selected_method');

                $resp['data'] = true;
                $resp['method'] = $method;
                $resp['methodDescr'] = $value['methodDescr'];
            } else {

                $resp['data'] = false;
            }

            wp_send_json_success($resp);
        }

        /**
         * 
         */
        public function addSettingsPage() {

            // Регистрируем пункт меню плагина
            add_menu_page('Int-Leads', 'Int Creating-leads', 'manage_options', 'int-send-leads', array($this, 'settingsPage'), 'data:image/x-icon;base64,AAABAAEAEBAAAAEAIABoBAAAFgAAACgAAAAQAAAAIAAAAAEAIAAAAAAAAAQAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAdAAA/3cAAP93AAD/dwAA/3AAAP8AAAAAAAAAAAAAAAAAAAAA6OXkCQAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAHQAAP93AAD/dwAA/3cAAP9wAAD/AAAAAAAAAAAAAAAAAAAAAJdHR83Mrq1YAAAAAAAAAAAAAAAAAAAAAAAAAAB0AAD/dwAA/3cAAP93AAD/cAAA/wAAAAAAAAAAAAAAAAAAAABzAAD9dQYG/6dpabjbzMo4AAAAAAAAAAAAAAAAdAAA/3cAAP93AAD/dwAA/3AAAP8AAAAAAAAAAAAAAAAAAAAAegoK9HUAAP9vAAD/fxoa+7uPjpbu8vAXAAAAAHQAAP93AAD/dwAA/3cAAP9wAAD/AAAAAAAAAAAAAAAAAAAAAHoKCvR2AAD/dwAA/3QAAP9vAAD/kDg44raBgbJ3AwP/dwAA/3cAAP93AAD/cAAA/wAAAAAAAAAAAAAAAAAAAAB5CAj1dgAA/3cAAP93AAD/dwAA/3IAAP9zAAD/dwEB/3cAAP93AAD/dwAA/3EAAP8AAAAAAAAAAAAAAAAAAAAAeAgI+3EAAP93AAD/dwAA/3cAAP93AAD/dwAA/3cAAP93AAD/dwAA/3cAAP9wAAD/AAAAAAAAAAAAAAAAAAAAAMampGCGKSnobwAA/3YAAP93AAD/dwAA/3cAAP93AAD/dwAA/3cAAP93AAD/cAAA/wAAAAAAAAAAAAAAAAAAAAAAAAAA49/dHqltbKh1BQX/cgAA/3cAAP93AAD/dwAA/3cAAP93AAD/dwAA/3AAAP8AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAxqakWYsxMeJvAAD/dQAA/3cAAP93AAD/dwAA/3cAAP9xAAD/AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAADh2dcZsHh4oHcJCf9xAAD/dwAA/3cAAP93AAD/cQAA/wAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAADUwL9Skj8+3G8AAP90AAD/dwAA/3EAAP8AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAOvu7BW4i4qZexER/3AAAP9xAAD/AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAN3T0UuYTEzYaQAA/wAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA8vr4ELqPjpsAAAAAAAAAAAAAAAAAAAAA/g8AAP4PAAB+DwAAHg8AAAYPAAAADwAAAA8AAAAPAACADwAAwA8AAPAPAAD4DwAA/g8AAP8PAAD/zwAA/+8AAA==', 90
            );

            // Регистрируем страницу плагина
            $page = add_submenu_page(
                    'int-send-leads', // Родительская страница меню
                    'Доп. параметры', // Название пункта меню
                    'Доп. Параметры', // Заголовок страницы
                    'manage_options', // Возможность, определяющая уровень доступа к пункту
                    'int-leads-option-page', // Ярлык (часть адреса) страницы плагина
                    array($this, 'leadSettingsPage')  // Функция, которая выводит страницу
            );

            // Используем зарегистрированную страницу для загрузки скрипта
            add_action('admin_print_scripts-' . $page, 'enqueue_adnin_scripts');
        }

        /**
         * 
         */
        public function settingsPage() {

            $field_option = self::intGetOption('int_selected_method');

            $choceMethod = $field_option['selectedMethod'];

            $toRoistatApi = isset($field_option['toRoistat']['apiKey']) ? $field_option['toRoistat']['apiKey'] : '';
            $toRoistatUrl = isset($field_option['toRoistat']['handlerUrl']) ? $field_option['toRoistat']['handlerUrl'] : '';

            $toBitrix24Api = isset($field_option['toBitrix24']['apiKey']) ? $field_option['toBitrix24']['apiKey'] : '';
            $toBitrix24Url = isset($field_option['toBitrix24']['handlerUrl']) ? $field_option['toBitrix24']['handlerUrl'] : '';



            if ($choceMethod == 'toBitrix24') {
                $choceSelectorBitrix = 'checked';
            } elseif ($choceMethod == 'toRoistat') {
                $choceSelectorRoistat = 'checked';
            } else {
                
            }
            $usingMethod = $field_option['methodDescr'];
            ?>
            <section class='int-settings-page-wrapp'>

                <h1>Настройки плагина лидогенерации СF7 to Bitrix24</h1>

                <div class="__selected">
                    <h2>Выбирите способ отправки данных с форм в лиды.</h2>
                    <ul class="__selected__ul-tabs">
                        <li data-fid="#toRoistat_form" data-choice="toRoistat">
                            <label>
                                <input name="lead_generate" type="radio" value="toRoistat" <?= $choceSelectorRoistat; ?>>
                                <span>Через Roistat, в Bitrix24</span>
                            </label>
                        </li>
                        <li data-fid="#toBitrix24_form" data-choice="toBitrix24">
                            <label>
                                <input name="lead_generate" type="radio" value="toBitrix24" <?= $choceSelectorBitrix; ?>>
                                <span>Через webhook Bitrix</span>
                            </label>
                        </li>
                    </ul>
                </div>
                <hr>
                <div class='selected-method'>
                    <h3><?= $usingMethod ?></h3>
                    <div id="toRoistat_form" class='selected-method__fields'>
                        <label>API ключ<input class='api_key_input' type="text" value='<?= $toRoistatApi ?>'></label>
                        <label>URL аккаунта<input class='akk_url_input' type="text" value='<?= $toRoistatUrl ?>'></label>
                        <br>
                        <div class="method-instruction">
                            
                        </div>
                    </div>
                    <div id="toBitrix24_form" class='selected-method__fields'>
                        <label>ID аккаунта<input class='api_key_input' type="text" value='<?= $toBitrix24Api ?>'></label>
                        <label>URL вебхука<input class='akk_url_input' type="text" value='<?= $toBitrix24Url ?>'></label>
                        <br>
                        <div class="method-instruction">
                            
                        </div>
                    </div>

                </div>
                <div class='ajax_save_lead_params'>
                    <span class='ajax_save_lead_params__btn'>Сохранить.</span>
                    <span class='ajax_save_lead_params__preloader'></span>
                </div>
                
                <div class="plugin-instruction">
                    <br>
                <hr>
                <br>
                    <h3>Общая инструкция</h3>
                    <div class="plugin-instruction__content">
                        <blockquote>
                        <h3>Инструкция метода лидогенирации, через Roistat</h3>
                            <div class="method-instruction__content">
                                <p>Заполните обязательное поле "URL вебхука", по которому будут отправлятся данные с форм в лиды.</p>
                                <p>Пример апи ключа<i> MTI3NDk6MTQ0NDQ6ZDRkMGNiMzUyZTk1ZGNlYzNhNGFjMTE1NDE5YzEwXXX=</i></p>
                                <p>URL для отправки данных лида<i> https://cloud.roistat.com/api/proxy/1.0/leads/add?</i></p>
                                <p>Инструкция: "Передача заявок в CRM через Roistat (проксилид)" - <i><a href="http://help.roistat.com/pages/viewpage.action?pageId=5898354" target="_blank">http://help.roistat.com/pages/viewpage.action?pageId=5898354</a></i></p>
                            </div>
                        </blockquote>
                        <blockquote>
                            <h3>Инструкция метода лидогенирации, через webhook</h3>
                            <div class="method-instruction__content">
                                <p>Заполните обязательное поле "URL вебхука", по которому будут отправлятся данные с форм в лиды.</p>
                                <p>Формат URL : https://akk-name.bitrix24.ru/rest/user-id/xxxxxx/crm.lead.add/ <i>не забудьте слешь в конце URL</i></p>
                                <ol>
                                    <li><p><i>akk-name</i> - название битрикс аккаунта</p></li>
                                    <li><p><i>xxxxxx</i> - код авторизации вебхука</p></li>
                                    <li><p><i>user-id</i> - id пользователя на которого зарегистрирован вебхук входящий</p></li>
                                    <li><p><i>crm.lead.add</i> - метод.обработчик(используем его) </p></li>
                                    <li><p>Ссылка на инструкцию, как создать входящий вебхук <i><a href="https://helpdesk.bitrix24.ru/open/5408147/" target="_blank">https://helpdesk.bitrix24.ru/open/5408147/</a></i> </p></li>
                                </ol>
                            </div>
                        </blockquote>
                    </div>
                </div>
            <?php //submit_button('Сохранить параметры');   ?>
            </section>
            <?php
        }

        /**
         * 
         */
        static function leadSettingsPage() {
            ?>

            <?php
        }

    }

    $intIndex = new IntIndexSendLeadsToRoistat();
}

