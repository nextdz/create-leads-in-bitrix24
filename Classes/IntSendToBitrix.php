<?php

namespace Integrus\RoistatLeads;

if (!class_exists('IntSendToBitrix')) {

    class IntSendToBitrix
    {

        private $API_KEY,
                $API_URL,
                $OPTION_NAME;
        private $bitrixArrayData;
        public $idForm;

        public function __construct() {
            $this->idForm = get_option('id_success_form', 1);


            add_filter('wpcf7_posted_data', array($this, 'filter_wpcf7_posted_data'), 10, 1);
//            add_action('wpcf7_before_send_mail', array($this, 'action_wpcf7_before_send_mail'), 10, 1); // Использовалось для отладки
            add_filter('wpcf7_ajax_json_echo', array($this, "getStatus"), 10, 1);

            add_shortcode('bitrix_test_data', array($this, 'testShortcode'));
        }

        /**
         * 
         * @param type $optionName
         */
        public function setOptionName($optionName = 'deff') {
            $this->OPTION_NAME = $optionName;
        }

        /**
         * URL аккаунта
         * 
         * @param type $api_url
         * @return boolean
         */
        public function setApiUrl($api_url = '') {
            if (!empty($api_url)) {
                $this->API_URL = $api_url;
                return true;
            } else {
                return false;
            }
        }

        /**
         * API ключ 
         * 
         * @param type $api_key
         * @return boolean
         */
        public function setApiKey($api_key = '') {
            if (!empty($api_key)) {
                $this->API_KEY = $api_key;
                return true;
            } else {
                return false;
            }
        }

        /**
         * Получаем гео данные
         * @return array
         */
        private function massageGeoLocation($ip) {
            $resp = [];
            $sxGeoPath = realpath(dirname(__FILE__) . '/..') . '/libs/SxGeo/SxGeo.php';

            if (file_exists($sxGeoPath)) {
                require_once $sxGeoPath;
                if (class_exists('SxGeo')) {
                    try {

                        $sxGeoObj = new \SxGeo('SxGeo.dat', SXGEO_BATCH | SXGEO_MEMORY);

                        $country_code = $sxGeoObj->getCityFull($ip);
                        
                        $resp['country'] = isset($country_code['country']['name_ru']) ? $country_code['country']['name_ru'] : '';
                        $resp['region'] = isset($country_code['region']['name_ru']) ? $country_code['region']['name_ru'] : '';
                        $resp['city'] = isset($country_code['city']['name_ru']) ? $country_code['city']['name_ru'] : '';
                    
                        
                    } catch (Exception $e) {
                        error_log('Ошибка при содании обекта "$sxGeoObj" класса SxGeo : ' . $e->getMessage() . "\n");
                    }
                }
            }
            
            return $resp;
        }

        /**
         * Собираем данные которые отправляются формой Contact Form 7
         * заполняем переменную $roistatData
         * 
         * @param array $posted_data
         * @return string
         */
        public function filter_wpcf7_posted_data($massage_content) {

            $posted_data = $massage_content;

            $idForm = get_option('id_success_form');

            $_page = esc_url($_SERVER['REQUEST_URI']);
            $_url = esc_url("http://" . $_SERVER['HTTP_HOST'] . $_page);
            $_ip = esc_attr($_SERVER["REMOTE_ADDR"]);
            
            // get geo data
            $geoData = self::massageGeoLocation($_ip);
            $country = isset($geoData['country']) ? $geoData['country'] : '';
            $region = isset($geoData['region']) ? $geoData['region'] : '';
            $city = isset($geoData['city']) ? $geoData['city'] : '';

            
            $addition_info = "\r\n \r\n ---------------\r\n "
                    . "\r\n Отправлено со страницы: {$_url}";

            $posted_data['your-message'] .= $addition_info;

            $lidComment = $posted_data['your-message'];
            $lidEmail = empty($posted_data['your-email']) ? '' : $posted_data['your-email'];
            $lidTel = empty($posted_data['your-tel']) ? '' : $posted_data['your-tel'];

            $altName = empty($lidEmail) ? $lidTel : $lidEmail; // Альт текст для поля имя.
            $lidName = empty($posted_data['your-name']) ? $altName : $posted_data['your-name'];

            // проверяем и передаем статус чекбокса "подписаться"
            if (isset($posted_data["_mc4wp_subscribe_contact-form-7"])) {
                if ($posted_data["_mc4wp_subscribe_contact-form-7"] == 1) {
                    $subscribe_val = 'Y';
                } elseif ($posted_data["_mc4wp_subscribe_contact-form-7"] == 0) {
                    $subscribe_val = "N";
                } else {
                    $subscribe_val = '';
                }
            } else {
                $subscribe_val = '';
            }

            // Формируем поля для отправки в лид
            $this->bitrixArrayData = [
                'fields' => [
                    'TITLE' => "{$city}, {$country} | " . esc_url($_SERVER['REQUEST_URI']),
                    'COMMENTS' => $lidComment,
                    'NAME' => $lidName,
                    'EMAIL' => [['TYPE' => 'VALUE', 'VALUE' => $lidEmail, 'VALUE_TYPE' => 'WORK']],
                    'PHONE' => [['TYPE' => 'VALUE', 'VALUE' => $lidTel, 'VALUE_TYPE' => 'WORK']],
                    'ADDRESS_CITY' => $city,
                    'UF_CRM_1543926559887' => $_ip,
                    'UF_CRM_1543926600491' => $_url,
                    'ADDRESS_COUNTRY' => $country,                      // Страна
//                    'ADDRESS_PROVINCE' => '| Область: '.$region.' ',    // Область
                    'UF_CRM_1455891261' => $city,                       // Город
                    'ADDRESS_CITY' => $city,                            // Город
                    'SOURCE_ID' => $_page,
                    'UF_CRM_1540382435686' => $lidEmail,                //доп. поле email
                    'UF_CRM_1544447664073' => $subscribe_val
                ],
                'params' => ['REGISTER_SONET_EVENT' => 'Y']
            ];
            update_option('id_success_form', ++$idForm);
            return $massage_content;
        }

        /**
         * Обновляем переменную в БД хранящую заголовок лида,
         * @param type $contact_form
         */
        public function action_wpcf7_before_send_mail($contact_form) {
//            update_option('bitrixArrayData_mail_data', $contact_form);  // Использовалось для отладки
        }

        /**
         * 
         * @param type $items
         * @param type $result
         * @return type
         */
        public function getStatus($items, $result) {

            if ($items['mailSent']) {


//                update_option('bitrixArrayData', $this->bitrixArrayData); //для отладки

                $this->bitrixResp = json_decode(file_get_contents($this->API_URL . '?' . http_build_query($this->bitrixArrayData)), 1);

//                update_option('bitrixArrayData_resp', $this->bitrixResp); //для отладки

                error_log(get_class($this) . " - Лид создан");
            } else {

                error_log(get_class($this) . " - Лид не создан");
            }

            return $items;
        }

        /**
         * 
         * Отладочный шорткод
         */
        public function testShortcode() {
            $res = '';

            $_mail_data = get_option('bitrixArrayData_mail_data');
            $data_resp = get_option('bitrixArrayData_resp');
            $data = get_option('bitrixArrayData');
            $data2 = $this->idForm;

            $res .= '<pre>';
            ob_start();
//            var_dump($_mail_data);
            echo '<hr>';
//            var_dump($data_resp);
            echo '<hr>';
            var_dump($data);
            echo '<hr>';
            echo $this->API_URL . '?' . http_build_query($data);
            echo '<hr>';
            var_dump($data2);

            $res .= ob_get_contents();
            ob_end_clean();
            $res .= '</pre>';

            return $res;
        }

    }

}
