jQuery(document).ready(function($){

    var intLeadsObj = {};
    
    console.log(ajaxDataObj);
    
    $('.__selected__ul-tabs input[name=lead_generate]:checked').parents('li').addClass('__selected__active');
    
    function sell(e){
        var $this = $('.__selected__ul-tabs input[name=lead_generate]:checked');
        var tabID = $('.__selected__ul-tabs input[name=lead_generate]:checked').parents('li').attr('data-fid');
        
        $('.__selected__ul-tabs li, .selected-method__fields').removeClass('__selected__active');
        
        $this.parents('li').addClass('__selected__active');
        $(tabID).addClass('__selected__active');
    }
    
    intLeadsObj.intLeadsInputValidate = function(formID){
        
        intLeadsObj.intLeadsInputValidate.error = false;
        
        if($(formID+' .api_key_input').val().length > 10){
            $(formID+' .api_key_input').removeClass('inputError');
            intLeadsObj.intLeadsInputValidate.error = false;
        }else{
            $(formID+' .api_key_input').addClass('inputError');
            intLeadsObj.intLeadsInputValidate.error = true;
        }
        
        if($(formID+' .akk_url_input').val().length > 10){
            $(formID+' .akk_url_input').removeClass('inputError');
            intLeadsObj.intLeadsInputValidate.error = false;
        }else{
            $(formID+' .akk_url_input').addClass('inputError');
            intLeadsObj.intLeadsInputValidate.error = true;
        }
        
    }
    
    $('.__selected__ul-tabs input[name=lead_generate]').on('change', function(){
        
       sell(this);
        
    });
    
    
    sell();
    
    $('.selected-method__fields').on('keypress','.inputError', function(){
        let id = $(this).parents('.selected-method__fields').attr('id');
        
        if(!intLeadsObj.intLeadsInputValidate('#'+id)){
            $('.ajax_save_lead_params__preloader').text('');
            $('.ajax_save_lead_params__preloader').removeClass('__error');
        }
    });
    
    $('.ajax_save_lead_params__btn').on('click', function(){
        
        
        var selectedMethod = $('.__selected__ul-tabs input[name=lead_generate]:checked').val();

        var formID = $('.__selected__ul-tabs input[name=lead_generate]:checked').parents('li').attr('data-fid');
        var apiKey = $(formID+' .api_key_input').val();
        var handlerUrl = $(formID+' .akk_url_input').val();
        
        intLeadsObj.intLeadsInputValidate(formID);
        
        $(formID).find('.error_massge').remove();
        
        if(!intLeadsObj.intLeadsInputValidate.error){
            ;
        
        
            let $sendData = new FormData();

            $sendData.append('action', 'selected_method');
            $sendData.append('nonce', ajaxDataObj.nonce);


            $sendData.append('selectedMethod', selectedMethod);
            $sendData.append('apiKey', apiKey);
            $sendData.append('handlerUrl', handlerUrl);

            $sendData.append('handler', selectedMethod);

            mainAdminAjaxHandler($sendData, this);
            
            
        }else{
            console.log((formID));
            $(formID).append('<span class="error_massge">Заполните все поля.</span>');
            
            $('.ajax_save_lead_params__preloader').text('Ошибка..');
            $('.ajax_save_lead_params__preloader').addClass('__error').removeClass('__sucsses').removeClass('__active');
        }
    });
    
//    function sucssesOk(preloader, text=''){
//        preloader.text(text);
//        preloader.addClass('__sucsses').removeClass('__error').removeClass('__active');
//    }
//    function sucssesError(preloader, text='Ошибка.'){
//        preloader.text(text);
//        preloader.addClass('__error').removeClass('__sucsses').removeClass('__active');
//    }
    
    function mainAdminAjaxHandler($sendData, $this) {
        
        var preloader = $('.ajax_save_lead_params__preloader');
        $(preloader).addClass('__active');
        

        
        $.ajax({
            url: ajaxDataObj.ajaxurl,
            type: 'POST',
            data: $sendData,
            cache: false,
            dataType: 'json',
            processData: false,
            contentType: false,
            success: function (respond) {
            if (respond.success) {
                console.dir(respond.data.data);
                console.dir(respond.data.methodDescr);
                
                if(respond.data.data && respond.data.methodDescr){
                  
                  $('.selected-method h3').html(respond.data.methodDescr);
                        console.dir('respond');
                }
                
                
                var timeoutID = window.setTimeout(function(){
                    preloader.text('');
                    preloader.addClass('__sucsses').removeClass('__error').removeClass('__active');
                }, 1000);
                
                
            }else {

                    preloader.text('Ошибка.' + respond.error);
                    preloader.addClass('__error').removeClass('__sucsses').removeClass('__active');
                    
            }

            },
            error: function (jqXHR, status, errorThrown) {
                console.dir('ОШИБКА AJAX запроса: ' + jqXHR, status, errorThrown);
                preloader.text('Ошибка. "error"');
                preloader.addClass('__error').removeClass('__sucsses').removeClass('__active');
            }
        });
    }
  
    
});