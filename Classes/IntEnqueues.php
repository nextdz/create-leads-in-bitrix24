<?php

namespace Integrus\RoistatLeads;

if (!class_exists('IntEnqueues')) {

    class IntEnqueues
    {
       
        private $NONCE;

        public function __construct() {

            add_action('wp_enqueue_scripts', array($this, 'enqueue_scripts'));
            if (is_admin())
                add_action('init', array($this, 'enqueue_adnin_scripts'));
        }
        
        /**
         * Запись NONCE
         */
        public function setNonce($nonce = 'deff') {
            if ($nonce != 'deff') {
                $this->NONCE = $nonce;
                return true;
            } else {
                $this->NONCE = $nonce;
                return false;
            }
        }

        /**
         * Перадаем данные в скрипт
         */
        private function localize_script_optoons() {

            $opt['ajaxurl'] = admin_url('admin-ajax.php');
            $opt['nonce'] = wp_create_nonce($this->NONCE);
            return $opt;
        }

        /**
         * Админ скрипты\стили
         */
        public function enqueue_adnin_scripts() {

            wp_register_script('int-rs-admin-js', plugins_url('../assets/admin/js/js.js', __FILE__));
            wp_enqueue_script('int-rs-admin-js');

            $opt = self::localize_script_optoons();
            wp_localize_script('int-rs-admin-js', 'ajaxDataObj', $opt);

            wp_register_style('int-rs-admin-css', plugins_url('../assets/admin/css/style.css', __FILE__));
            wp_enqueue_style('int-rs-admin-css');
        }

        /**
         * Подключаем скрипты и стили
         */
        public function enqueue_scripts() {

            wp_register_script('int-rs-js', plugins_url('../assets/frontend/js/script.js', __FILE__));
            wp_enqueue_script('int-rs-js');

            $opt = self::localize_script_optoons();
            wp_localize_script('int-rs-js', 'ajaxDataObj', $opt);

            wp_register_style('int-rs-css', plugins_url('../assets/frontend/css/style.css', __FILE__));
            wp_enqueue_style('int-rs-css');
        }

    }

}