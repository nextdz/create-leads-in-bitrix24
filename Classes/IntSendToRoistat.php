<?php

namespace Integrus\RoistatLeads;

if (!class_exists('IntSendToRoistat')) {

    class IntSendToRoistat
    {
        private $API_KEY,
                $API_URL,
                $OPTION_NAME;
        
        private $roistatData,
                $roistatResp;
        
        public $idForm;

        public function __construct() {
            $this->idForm = get_option('id_success_form', 1);
                        
            add_filter('wpcf7_posted_data', array($this, 'filter_wpcf7_posted_data'), 10, 1);
//            add_action( 'wpcf7_before_send_mail', array($this, 'action_wpcf7_before_send_mail'), 10, 1 );
            add_filter( 'wpcf7_ajax_json_echo', array($this, "getStatus"), 10, 1);
            
            add_shortcode('roist_test_data', array($this, 'testShortcode'));
        }
        /**
         * 
         * @param type $optionName
         */
        public function setOptionName($optionName = 'deff'){
            $this->OPTION_NAME = $optionName;
        }
        
        /**
         * URL аккаунта
         * 
         * @param type $api_url
         * @return boolean
         */
        public function setApiUrl($api_url = ''){
            if(!empty($api_url)){
               $this->API_URL = $api_url;  
               return true;
            }else{
                return false;
            } 
        }
        
        /**
         * API ключ 
         * 
         * @param type $api_key
         * @return boolean
         */
        public function setApiKey($api_key = ''){
            if(!empty($api_key)){
               $this->API_KEY = $api_key;  
               return true;
            }else{
                return false;
            } 
        }

        /**
         * Собираем данные которые отправляются формой Contact Form 7
         * заполняем переменную $roistatData
         * 
         * @param array $posted_data
         * @return string
         */
        public function filter_wpcf7_posted_data($massage_content) {

            $posted_data = $massage_content;
            
            $idForm = get_option('id_success_form');
            
            $_page = esc_url( $_SERVER['REQUEST_URI'] );
            $_url = esc_url( "http://" . $_SERVER['HTTP_HOST'] . $_page );
            $_ip = esc_attr($_SERVER["REMOTE_ADDR"]);
            
            $addition_info =  "\r\n \r\n ___________________________________________\r\n "
                            . "\r\n Номер Roistat визита: {visit}"
                            . "\r\n Страница с которой перешел посетитель: {referrer}"
                            . "\r\n IP  посетителя: {$_ip}"
                            . "\r\n Отправлено со страницы: {$_url}";

            $posted_data['your-message'] .= $addition_info;

            $lidComment = $posted_data['your-message'];
            $lidEmail = empty($posted_data['your-email']) ? '' : $posted_data['your-email'];
            $lidTel = empty($posted_data['your-tel']) ? '' : $posted_data['your-tel'];

            $altName = empty($lidEmail) ? $lidTel : $lidEmail; // Альт текст для поля имя.
            $lidName = empty($posted_data['your-name']) ? $altName : $posted_data['your-name'];
            

            if (isset($posted_data["_mc4wp_subscribe_contact-form-7"])) {
                if ($posted_data["_mc4wp_subscribe_contact-form-7"] == 1) {
                    $subscribe_val = 'Y';
                } elseif ($posted_data["_mc4wp_subscribe_contact-form-7"] == 0) {
                    $subscribe_val = "N";
                } else {
                    $subscribe_val = '';
                }
            } else {
                $subscribe_val = '';
            }

            $this->roistatData = array(
                'roistat' => isset($_COOKIE['roistat_visit']) ? $_COOKIE['roistat_visit'] : null,
                'key' => $this->API_KEY, 
                'title' => '{city}, {country} | '.esc_url($_SERVER['REQUEST_URI']),
                'comment' => $lidComment,
                'name' => $lidName,
                'email' => $lidEmail,
                'phone' => $lidTel,
                'is_need_callback' => '0', // Для автоматического использования обратного звонка при отправке контакта и сделки нужно поменять 0 на 1
                'LEAD_FORMATTED_NAME' => '{city}, {country} ~ '.esc_url($_SERVER['REQUEST_URI']).'',
                'fields' => array(
                    'LEAD_FORMATTED_NAME' => '{city}, {country} | '.esc_url($_SERVER['REQUEST_URI']),
                    'ADDRESS_COUNTRY' => '{country}',   // Страна
                    'UF_CRM_1455891261' => '{city}',    // Город, доп. поле
                    'ADDRESS_CITY' => '{city}',         // Город
                    'UF_CRM_1543926559887' => $_ip,
                    'UF_CRM_1543926600491' => $_url,
                    'SOURCE_ID' => $_page,
                    'UF_CRM_1544447664073' => $subscribe_val
                ),
            );
            update_option('id_success_form', ++$idForm);
//            update_option($this->OPTION_NAME.'_mail_data', $massage_content ); // Использовалось для отладки
            return $massage_content;
        }
        
        /**
         * Не используется(Обновляем переменную, хранящую заголовок лида)
         * @param type $contact_form
         */
        public function action_wpcf7_before_send_mail($contact_form) {
//            update_option($this->OPTION_NAME.'_mail_data', $contact_form ); // Использовалось для отладки
//            $this->roistatData['fields']['UF_CRM_1544447664073'] = '{city}, {country} | '.esc_url($_SERVER['REQUEST_URI']);
        }
        
        /**
         * 
         * @param type $items
         * @param type $result
         * @return type
         */
        public function getStatus($items, $result) {

            if ($items['mailSent']) {
                
                
//                update_option($this->OPTION_NAME, $this->roistatData ); //для отладки
                
                $this->roistatResp = file_get_contents($this->API_URL . http_build_query($this->roistatData));
                
//                update_option($this->OPTION_NAME.'_resp', $this->roistatResp ); //для отладки
                
                error_log(get_class($this)." - Лид создан");
                
            } else {

                error_log(get_class($this)." - Лид не создан");
            }

            return $items;
        }
        
        /**
         * 
         * Отладочный шорткод
         */
        public function testShortcode(){
            $res = '';
            
            $_mail_data = get_option($this->OPTION_NAME.'_mail_data');
            $data_resp  = get_option($this->OPTION_NAME.'_resp');
            $data       = get_option($this->OPTION_NAME);
            $data2      = $this->idForm;
            
            $res .= '<pre>';
            ob_start();
            var_dump($_mail_data);
            echo '<hr>';
            var_dump(json_decode($data_resp));
            echo '<hr>';
            var_dump($data);
            echo '<hr>';
            echo $this->API_URL .  http_build_query($data);
            echo '<hr>';
            var_dump($data2);
            
            $res .= ob_get_contents();
            ob_end_clean();
            $res .= '</pre>';
            
            return $res;
        }
        
    }

}

